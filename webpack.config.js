const path = require('path');
const autoprefixer = require('autoprefixer');
// this plugin applys all other file transformations, and takes the result of it for greater transformation
const HtmlWebpackPlugin = require('html-webpack-plugin');
let whiteListedModules = ['vue','axios','vue-electron', 'vue-router', 'vuex', 'vuex-electron','element-ui']
// node syntax that will export entire code as object
module.exports = {
    // decide if webpack should add some optimization to project
    mode: 'development',
    // decide where to start reading code
    entry: {
        main: path.resolve(__dirname, './src/index.js')
    },
    // decide where to put the final app output
    output: {
        // creates an absolute path compared to this current directory (where webpack.config is located)
        path: path.resolve(__dirname, 'dist'),
        // name of the file where all of the js code will be bundled
        filename: '[name].bundle.js',
        publicPath: ''
    },
    // controlls how source maps are generated, helps for debugging
    devtool: 'eval-nosources-cheap-module-source-map',
    module: {
        rules: [
            // each rule is an object
            {
                // use regEx to decide which files are affected with this rule
                // here we have every file that ends with .js
                test: /\.js$/,
                // this tells webpack which tool takes over for this file
                // here we use babel-loader which we installed as a @babel packace
                // we decide what will babel do in .babelrc file which will babe automaticaly look for
                loader: 'babel-loader',
                // which files to exclude
                exclude: /node_modules/,
            },
            {
                test: /\.s[ac]ss$/,
                exclude: /node_modules/,
                // to add multiple loaders, use "use"
                use: [
                    // each loader is represented as object
                    // style loader is responsible for injecting imported css
                    { loader: 'style-loader' },
                    // css loader is reposible for handling css imports
                    // for additional configuration add options key
                    { loader: 'css-loader', options: {
                            importLoaders: 1,
                            modules: {
                                localIdentName: '[name]__[local]__[hash:base64:5]'
                            }
                        } 
                    },
                    // sass loader will handle sass written code in scss files
                    { loader: 'sass-loader',
                        options: {
                            // this will indicate that we will use `dart-sass`
                            implementation: require('sass')
                        }
                    },
                    // this loader will help with autoprefexes
                    { loader: 'postcss-loader', 
                      options: {
                        // ident: 'postcss',
                        // in this array we execute plugins (that we install) we want to run over our css code
                        // for autoprefixer to work we need to add browserlist property to package.json
                            postcssOptions: {
                                plugins: [
                                    [
                                       'autoprefixer'
                                    ]
                                ]
                            }
                        }
                    }
                ]
            },
            {
                // for handling image files
                test: /\.(png|jpe?g|gif)$/,
                // example of inline configuration
                loader: 'url-loader', options: {
                    limit: '8000',
                    name: 'images/[name].[ext]'
                }
            },
        ]
    },
    // we add this AFTER module, array of plugins
    plugins: [
        new HtmlWebpackPlugin({
            // this should generate index.html in specified loaction and inject all imports in body (we can change to head or etc)
            template: __dirname + '/src/index.html',
            filename: 'index.html',
            inject: 'body'
        })
    ],
};