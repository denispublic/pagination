import React, { useState, useEffect } from 'react';
import axios from 'axios';
import Posts from './components/Posts';
import Pagination from './components/Pagination'
import Auxiliarty from './hoc/Auxiliarty';

const App = () => {
    const [posts, setPosts] = useState([]);
    const [loading, setLoading] = useState(false);
    const [currentPage, setCurrentPage] = useState(1);
    const [postsPerPage, setPostsPerPage] = useState(10);
    const [postsPerPageArr] = useState([5, 10, 15, 20]);
    
    useEffect(() => {
        const getPosts = async () => {
            setLoading(true);
            const res = await axios.get('https://jsonplaceholder.typicode.com/comments',{
                params: {
                  _limit: 50
                 }
              });
            setPosts(res.data);
            setLoading(false);
        }
        getPosts();
    }, [])
    console.log(posts)

    // get index of last post on current page
    const lastPostIndex = currentPage * postsPerPage;
    // get index of first post on current page
    const firstPostIndex = lastPostIndex - postsPerPage;
    // get posts on current page
    const currentPosts = posts.slice(firstPostIndex, lastPostIndex);

    // paginate
    const paginate = pageNumber => setCurrentPage(pageNumber);
    const changePostsPerPage = number => setPostsPerPage(number);


    return (
        <Auxiliarty>
            <h2>Custom table pagination</h2>
            <table>
                <tbody>
                    <tr>
                        <th>Id</th>
                        <th>Title</th>
                        <th>Email</th>
                        <th>Content</th>
                    </tr>
                    <Posts posts={currentPosts} loading={loading}/>
                </tbody>
            </table>
            <Pagination postsPerPage={postsPerPage} totalPosts={posts.length} paginate={paginate} currentPage={currentPage}/>
        </Auxiliarty>
    );
};

export default App;