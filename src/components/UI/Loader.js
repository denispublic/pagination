import classes from './Loader.scss';

import React from 'react';

const Loader = () => <div className={classes.loader}>Loading...</div>;

export default Loader;