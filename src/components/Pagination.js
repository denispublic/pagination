import React from 'react';
import classes from './Pagination.scss'

const Pagination = ({postsPerPage, totalPosts, paginate, currentPage}) => {

    const pageNumbers = [];

    // filling page numbers array, 
    // use math ceil to get even number
    for(let i = 1; i<= Math.ceil(totalPosts / postsPerPage); i++) {
        pageNumbers.push(i);
    }

    const pagination = pageNumbers.map(num => (
        <li key={num} className={num === currentPage ? classes.Active : null}>
            <a href="#" onClick={() => {paginate(num)}}>
                {num}
            </a>
        </li>
    ))

    return (
        <ul className={classes.Ul}>
            {pagination}
        </ul>
    );
};

export default Pagination;