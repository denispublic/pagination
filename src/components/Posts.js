import React from 'react';
import Auxiliarty from '../hoc/Auxiliarty';
import Loader from './UI/Loader';
import "./Posts.scss";

const Posts = ({posts, loading}) => {

    if(loading) {
        return <tr><td><Loader /></td></tr>
    };

    const post = posts.map(post => (
            <tr key={post.id}>
                <th>{post.id}</th>
                <th>{post.name}</th>
                <th>{post.email}</th>
                <th>{post.body}</th>
            </tr>
        )
    )

    return (
         post
    );
};

export default Posts;